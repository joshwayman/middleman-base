###
# Compass
###

# use redcarpet for markdown
set :markdown_engine, :redcarpet

# Change Compass configuration
# compass_config do |config|
#   config.output_style = :compact
# end

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
# page "/path/to/file.html", :layout => false
#
# With alternative layout
# page "/path/to/file.html", :layout => :otherlayout
#
# A path which all have the same layout
# with_layout :admin do
#   page "/admin/*"
# end

# Proxy pages (http://middlemanapp.com/basics/dynamic-pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", :locals => {
#  :which_fake_page => "Rendering a fake page with a local variable" }

###
# Helpers
###

# contentful stuff
=begin
activate :contentful do |f|
    f.space         = { contentful: 'gsclu020tznb'}
    f.access_token  = 'bb257da787d2fa876bb54663d25b6d5e32b39d2d310cfbdb726623f9351beb58'
    f.cda_query     = { include: 1 }
    f.content_types = { author: '1kUEViTN4EmGiEaaeC6ouY	', cat: 'author', catVideos: 'catVideos'  }
    f.use_preview_api = false
end
=end

activate :contentful do |f|
    f.space         = { contentful: ENV['FOODTIMES_SPACE_ID'] }
    f.access_token  = ENV['FOODTIMES_API_KEY']
    f.cda_query     = { include: 1 }
    f.content_types = { styleGuide: 'styleGuide', pages: 'mainPage', content: 'pageContentBlocks'  }
    #f.content_types = { author: '1kUEViTN4EmGiEaaeC6ouY	', cat: 'author', catVideos: 'catVideos'  }
    f.use_preview_api = false #ENV['FOODTIMES_USE_PREVIEW']
end

# Automatic image dimensions on image_tag helper
# activate :automatic_image_sizes

# Reload the browser automatically whenever files change
configure :development do
   activate :livereload
    
    # the following code solves the problem of proxying API requests to another host
    # simulates what Netlify does with it's proxying solution
    # sourced from https://gist.github.com/nhemsley/9183023
    # should be re-written as an extension for middleman
    require 'lib/rails_proxy.rb'
    use ::Rack::RailsProxy
 end

# Methods defined in the helpers block are available in templates
# helpers do
#   def some_helper
#     "Helping"
#   end
# end

set :css_dir, 'stylesheets'

set :js_dir, 'javascripts'

set :images_dir, 'images'

# Build-specific configuration
configure :build do
  # For example, change the Compass output style for deployment
  activate :minify_css

  # Minify Javascript on build
  # activate :minify_javascript

  # Enable cache buster
  # activate :asset_hash

  # Use relative URLs
  # activate :relative_assets

  # Or use a different image path
  # set :http_prefix, "/Content/images/"
end


# create proxy pages for data files
if Dir.exist?(config.data_dir)

  # blog posts
    data.contentful.pages.each do |id, post|
        proxy "/#{post.slug}.html", '/page.html', locals: { post: post }, :ignore => true
  end
    
end


